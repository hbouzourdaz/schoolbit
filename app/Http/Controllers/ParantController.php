<?php

namespace App\Http\Controllers;

use App\Models\Parant;
use Illuminate\Http\Request;

class ParantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Parant  $parant
     * @return \Illuminate\Http\Response
     */
    public function show(Parant $parant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Parant  $parant
     * @return \Illuminate\Http\Response
     */
    public function edit(Parant $parant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Parant  $parant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parant $parant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Parant  $parant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parant $parant)
    {
        //
    }
}
