<?php

namespace App\Http\Controllers;

use App\Models\Ctype;
use Illuminate\Http\Request;

class CtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ctype  $ctype
     * @return \Illuminate\Http\Response
     */
    public function show(Ctype $ctype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ctype  $ctype
     * @return \Illuminate\Http\Response
     */
    public function edit(Ctype $ctype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ctype  $ctype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ctype $ctype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ctype  $ctype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ctype $ctype)
    {
        //
    }
}
