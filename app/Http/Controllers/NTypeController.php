<?php

namespace App\Http\Controllers;

use App\Models\NType;
use Illuminate\Http\Request;

class NTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NType  $nType
     * @return \Illuminate\Http\Response
     */
    public function show(NType $nType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NType  $nType
     * @return \Illuminate\Http\Response
     */
    public function edit(NType $nType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NType  $nType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NType $nType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NType  $nType
     * @return \Illuminate\Http\Response
     */
    public function destroy(NType $nType)
    {
        //
    }
}
