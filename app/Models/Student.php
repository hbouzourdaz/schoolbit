<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }
    public function notes()
    {
        return $this->hasMany(Note::class);
    }
    public function absences()
    {
        return $this->hasMany(Absence::class);
    }
    public function parent()
    {
        return $this->belongsTo(Parant::class);
    }
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function notices()
    {
        return $this->hasMany(Notice::class);
    }
}
