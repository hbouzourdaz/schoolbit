<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
    use HasFactory;
    public function subject_teacher()
    {
        return $this->belongsTo(SubjectTeacher::class);
    }
    public function teacher()
    {
        return $this->subject_teacher->teacher;
    }
    public function subject()
    {
        return $this->subject_teacher->subject;
    }

    public function group()
    {
        return belongsTo(Group::class);
    }
    public function classroom()
    {
        return $this->belongsTo(Classromm::class);
    }

    public function timing()
    {
        return $this->belongsTo(Timing::class);
    }

}
