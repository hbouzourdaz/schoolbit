<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    use HasFactory;
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function seance()
    {
        return $this->belongsTo(Seance::class);
    }

    public function timing()
    {
        return $this->seance->timing;
    }

    public function teacher()
    {
        return $this->seance->teacher;
    }

}
