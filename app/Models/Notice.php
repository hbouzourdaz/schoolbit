<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\Seance;

class Notice extends Model
{
    use HasFactory;
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    public function seance()
    {
        return $this->belongsTo(Seance::class);
    }
    public function teacher()
    {
        return $this->seance->teacher;
    }
    public function timing()
    {
        return $this->seance->timing;
    }
    public function subject()
    {
        return $this->seance->subject;
    }
}
