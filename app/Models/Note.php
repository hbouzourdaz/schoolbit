<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;
    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    
    public function subject_teacher()
    {
        return $this->belongsTo(SubjectTeacher::class);
    }
    public function Teacher()
    {
        return $this->subject_teacher->teacher;
    }
    public function Subject()
    {
        return $this->subject_teacher->subject;
    }
}
