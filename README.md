# Welcome team

## Clone this project
git clone https://bitbucket.org/Anouar-Barouchi/schoolbit
## Access to your project
cd SchoolBit
## Install Composer dependencies
composer install
## Create your own .env file
cp .env.example .env
## Generate your Encryption key
php artisan key:generate

## Install npm 
npm install

### Now after updating the .env file you can migrate or seed your database.

#### Hope it's useful, regards